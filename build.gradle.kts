import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.apache.tools.ant.taskdefs.condition.Os
import org.codehaus.groovy.ast.tools.GeneralUtils.args
import java.io.ByteArrayOutputStream

plugins {
    java
    kotlin("jvm") version "1.3.0"
}

repositories {
    mavenCentral()
}

dependencies {
    api("it.unibo.alchemist:alchemist:8.0.0-beta+0q8.31ea0")
    implementation(kotlin("stdlib-jdk8"))
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

sourceSets.getByName("main") {
    resources {
        srcDirs("src/main/protelis")
    }
}

task("runTests") {
    doLast {
        println("Done.")
    }
}
fun makeTest(
    file: String,
    name: String = file,
    sampling: Double = 1.0,
    time: Double = Double.POSITIVE_INFINITY,
    vars: Set<String> = setOf(),
    maxHeap: Long? = null,
    taskSize: Int = 1024,
    threads: Int? = null,
    debug: Boolean = false
) {
    val heap: Long = maxHeap ?: if (System.getProperty("os.name").toLowerCase().contains("linux")) {
        ByteArrayOutputStream()
        .use { output ->
            exec {
                executable = "bash"
                args = listOf("-c", "cat /proc/meminfo | grep MemAvailable | grep -o '[0-9]*'")
                standardOutput = output
            }
            output.toString().trim().toLong() / 1024
        }
        .also { println("Detected ${it}MB RAM available.") }  * 9 / 10
    } else {
        // Guess 16GB RAM of which 2 used by the OS
        14 * 1024L
    }
    val threadCount = threads ?: maxOf(1, minOf(Runtime.getRuntime().availableProcessors(), heap.toInt() / taskSize ))
    println("Running on $threadCount threads")
    task<JavaExec>("$name") {
        classpath = sourceSets["main"].runtimeClasspath
        classpath("src/main/protelis")
        main = "it.unibo.alchemist.Alchemist"
        maxHeapSize = "${heap}m"
        jvmArgs("-XX:+AggressiveHeap")
        if (debug) {
            jvmArgs("-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=1044")
        }
        File("data").mkdirs()
        args(
            "-y", "src/main/yaml/${file}.yml",
            "-t", "$time",
            "-e", "data/${name}",
            "-p", threadCount,
            "-i", "$sampling"
        )
        if (vars.isNotEmpty()) {
            args("-b", "-var", *vars.toTypedArray())
        }
    }
    tasks {
        "runTests" {
            dependsOn("$name")
        }
    }
}
makeTest(name="cloud", file = "vienna", sampling = 2.0, time = 665.0, vars = setOf("seed", "bandwidth", "propagationDelay", "baserange"), taskSize = 2200)
makeTest(name="edge", file = "vienna", sampling = 2.0, time = 665.0, vars = setOf("seed", "bandwidth", "accessPointCount", "baserange"), taskSize = 3200)
//makeTest(name="reference", file = "vienna", sampling = 10.0, time = 3600.0, vars = setOf("seed"), taskSize = 16 * 1024)
defaultTasks("runTests")
