#####################################################################
# Alchemist log file - simulation started at: 2019-03-16T04:34+0000 #
#####################################################################
# seed = 8.0, bandwidth = 316227.76601683797, propagationDelay = 0.001, baserange = 56.234132519034915
#
# The columns have the following meaning: 
# time neighbor_count[Sum] <value>@neighbor_count[Sum] packet_size[Mean] mean_delay[Mean] crowd[Sum] overcrowded[Sum] risk[Sum] 
0.0 NaN NaN NaN NaN NaN NaN NaN 
2.0006217106462856 6161.0 NaN 4996.38209752839 0.5012710755120564 1496.0 1.0 1496.0 
4.000647008867567 6162.0 NaN 4998.562458249833 0.5013043928811436 1492.0 5.0 1492.0 
6.000198348008885 6162.0 NaN 4995.770207080828 0.5013197458458124 1470.0 27.0 1470.0 
8.000634359756926 6162.0 NaN 4996.390113560455 0.5014553359419089 1469.0 28.0 1469.0 
10.000609061535645 6161.0 NaN 4994.869739478958 0.5015150830894175 1447.0 50.0 1447.0 
12.000583763314363 6164.0 NaN 4995.441549766199 0.5007922174392746 1458.0 39.0 1458.0 
14.000583763314363 6168.0 NaN 4994.273881095524 0.5010601803378288 1450.0 47.0 1450.0 
16.000198348008887 6170.0 NaN 4995.038076152305 0.5009314112235727 1453.0 44.0 1453.0 
18.000583763314367 6172.0 NaN 4993.899799599199 0.5009524740581239 1451.0 46.0 1451.0 
20.000583763314367 6178.0 NaN 4994.778891115565 0.5009565056894928 1454.0 43.0 1454.0 
22.000583763314367 6187.0 NaN 4993.766199064797 0.5024131971746181 1453.0 44.0 1453.0 
24.000583763314367 6190.0 NaN 4994.690714762859 0.5048961022303888 1453.0 44.0 1453.0 
26.000198348008887 6191.0 NaN 4993.579158316633 0.5042316875144316 1454.0 43.0 1454.0 
28.000583763314367 6192.0 NaN 4994.605210420842 0.5018853041981447 1454.0 43.0 1454.0 
30.000583763314367 6196.0 NaN 4993.59786239145 0.5022905712230046 1454.0 43.0 1454.0 
32.00058376331437 6213.0 NaN 4994.538410153641 0.5029038984773957 1454.0 43.0 1454.0 
34.00058376331436 6233.0 NaN 4993.723446893788 0.5039334885647483 1453.0 44.0 1453.0 
36.00019834800889 6222.0 NaN 4994.490313961256 0.5010361719291307 1453.0 44.0 1453.0 
38.00058376331436 6222.0 NaN 4993.600534402138 0.500940586754204 1453.0 44.0 1453.0 
40.00058376331436 6228.0 NaN 4994.583834335337 0.5013858001078995 1453.0 44.0 1453.0 
42.00058376331436 6245.0 NaN 4993.605878423514 0.5025977381059564 1453.0 44.0 1453.0 
44.00058376331436 6254.0 NaN 4994.487641950568 0.505549661179196 1453.0 44.0 1453.0 
46.00019834800889 6241.0 NaN 4993.670006680027 0.5016195575088018 1453.0 44.0 1453.0 
48.00058376331436 6234.0 NaN 4994.573146292585 0.5008203358412574 1453.0 44.0 1453.0 
50.00058376331436 6248.0 NaN 4993.613894455578 0.501203886645433 1453.0 44.0 1453.0 
52.00058376331436 6291.0 NaN 4994.562458249833 0.5043380021469369 1453.0 44.0 1453.0 
54.00058376331436 6276.0 NaN 4993.651302605211 0.5059528981632041 1451.0 46.0 1451.0 
56.00019834800889 6257.0 NaN 4994.41549766199 0.5019339711240182 1452.0 45.0 1452.0 
58.00058376331436 6256.0 NaN 4993.656646626587 0.5027428430579536 1452.0 45.0 1452.0 
60.00058376331436 6258.0 NaN 4994.503674014696 0.5024557791206258 1452.0 45.0 1452.0 
62.00042575550556 6272.0 NaN 1519.0674682698727 0.5077165294974982 732.0 49.0 315.0 
64.00042575550556 6284.0 NaN 1521.8490313961267 0.5107607159470382 563.0 55.0 310.0 
66.0001983480089 6267.0 NaN 1521.5978623914484 0.505824757730993 528.0 53.0 312.0 
68.00042575550556 6276.0 NaN 1521.6085504342004 0.5081499584942535 499.0 49.0 316.0 
70.00042575550556 6276.0 NaN 1524.705410821645 0.505483584233946 492.0 44.0 322.0 
72.00042575550556 6289.0 NaN 1524.6519706078818 0.5072551227286749 486.0 46.0 320.0 
74.00042575550556 6291.0 NaN 1524.627922511691 0.5080577244655542 484.0 46.0 320.0 
76.0001983480089 6303.0 NaN 1530.7601870407482 0.5107718498361528 486.0 44.0 324.0 
78.00042575550556 6283.0 NaN 1530.7682030728117 0.5112241769949742 483.0 44.0 324.0 
80.00042575550556 6278.0 NaN 1530.7040748163001 0.5054056971422031 482.0 44.0 324.0 
82.00042575550556 6282.0 NaN 1530.7895791583178 0.5064177825317663 482.0 45.0 323.0 
84.00042575550556 6271.0 NaN 1533.7955911823633 0.5062126525525452 483.0 44.0 325.0 
86.0001983480089 6256.0 NaN 1533.8944555778212 0.5080354644176055 480.0 46.0 323.0 
88.00042575550556 6246.0 NaN 1533.8463593854362 0.5045831219727059 473.0 53.0 316.0 
90.00042575550556 6255.0 NaN 1533.8383433533727 0.5072099214093985 480.0 47.0 322.0 
92.00042575550556 6262.0 NaN 1533.814295257182 0.5063795384761592 477.0 50.0 319.0 
94.00042575550556 6265.0 NaN 1533.8543754175003 0.507013743207743 480.0 47.0 322.0 
96.0001983480089 6256.0 NaN 1536.895123580494 0.5075605108473961 476.0 50.0 320.0 
98.00042575550556 6251.0 NaN 1536.9004676018724 0.5054606134530458 479.0 48.0 322.0 
100.00042575550556 6253.0 NaN 1536.8897795591183 0.5051676109484392 479.0 48.0 322.0 
102.00042575550556 6266.0 NaN 1536.9245156980637 0.5054482349519347 480.0 47.0 323.0 
104.00042575550556 6277.0 NaN 1536.8283233132931 0.5079958724998477 479.0 48.0 322.0 
106.0001983480089 6262.0 NaN 1536.865731462927 0.5090353949198605 478.0 49.0 321.0 
108.00042575550556 6250.0 NaN 1536.7882431529729 0.5088683774966621 477.0 49.0 321.0 
110.00042575550556 6236.0 NaN 1536.8470273881092 0.5076188011047552 482.0 44.0 326.0 
112.00042575550556 6252.0 NaN 1539.890447561791 0.5063808235619996 484.0 42.0 329.0 
114.00042575550556 6262.0 NaN 1539.9331997328002 0.5103388778697516 485.0 41.0 330.0 
116.0001983480089 6231.0 NaN 1539.871743486973 0.509471554539637 485.0 42.0 329.0 
118.00042575550556 6252.0 NaN 1543.0033400133607 0.5081479197605435 485.0 44.0 328.0 
120.00042575550556 6252.0 NaN 1546.0681362725456 0.5054533942499222 484.0 45.0 328.0 
122.00042575550556 6272.0 NaN 1546.092184368736 0.5057129790684886 487.0 42.0 331.0 
124.00042575550556 6286.0 NaN 1543.014028056112 0.5065001141839125 487.0 42.0 330.0 
126.0001983480089 6279.0 NaN 1542.9873079492334 0.5052995034907422 488.0 41.0 331.0 
128.00042575550555 6282.0 NaN 1542.9552438209755 0.5070660551648564 490.0 37.0 335.0 
130.00042575550555 6285.0 NaN 1539.9438877755515 0.5146279719658862 490.0 37.0 334.0 
132.00042575550555 6280.0 NaN 1539.9011356045426 0.5075502376412563 488.0 37.0 334.0 
134.00042575550555 6288.0 NaN 1536.9485637942544 0.5095824051481109 486.0 37.0 333.0 
136.0001983480089 6272.0 NaN 1530.7548430193729 0.510382651175184 486.0 37.0 331.0 
138.00042575550555 6271.0 NaN 1533.902471609888 0.5059281968917567 483.0 37.0 332.0 
140.00042575550555 6279.0 NaN 1536.8550434201725 0.5112820288038776 482.0 38.0 332.0 
142.00042575550555 6276.0 NaN 1543.1022044088184 0.5069457749957341 478.0 43.0 329.0 
144.00042575550555 6292.0 NaN 1543.043420173682 0.51175584588617 478.0 43.0 329.0 
146.0001983480089 6277.0 NaN 1543.0113560454245 0.5055995861654313 473.0 48.0 324.0 
148.00042575550555 6279.0 NaN 1549.2077488309944 0.5072355793971038 472.0 49.0 325.0 
150.00042575550555 6276.0 NaN 1549.1730126920515 0.5136079532246688 475.0 47.0 327.0 
152.00042575550555 6268.0 NaN 1539.935871743488 0.5088616633581352 478.0 45.0 326.0 
154.00042575550555 6262.0 NaN 1536.8737474949912 0.5089356903447578 473.0 47.0 323.0 
156.0001983480089 6252.0 NaN 1539.8690714762847 0.5091020751174262 472.0 46.0 325.0 
158.00042575550555 6258.0 NaN 1539.8423513694058 0.5101066181087568 474.0 46.0 325.0 
160.00042575550555 6240.0 NaN 1539.847695390782 0.5088756221411148 472.0 49.0 322.0 
162.00042575550555 6239.0 NaN 1539.8824315297275 0.50829600585413 472.0 47.0 324.0 
164.00042575550555 6242.0 NaN 1542.971275885103 0.5088304069589838 473.0 46.0 326.0 
166.0001983480089 6222.0 NaN 1542.947227788909 0.5073054011882214 476.0 43.0 329.0 
168.00042575550555 6224.0 NaN 1542.936539746158 0.5079997933135536 476.0 43.0 329.0 
170.00042575550555 6233.0 NaN 1539.8450233800936 0.5127377409263241 478.0 42.0 329.0 
172.00042575550555 6224.0 NaN 1546.0708082832336 0.5092627945682858 481.0 42.0 331.0 
174.00042575550555 6221.0 NaN 1546.0574482297911 0.5083380154457132 482.0 42.0 331.0 
176.0001983480089 6207.0 NaN 1542.8964595858365 0.5113618797279613 482.0 40.0 332.0 
178.00042575550555 6197.0 NaN 1546.0681362725456 0.5062144980007982 480.0 41.0 332.0 
180.00042575550555 6203.0 NaN 1546.052104208418 0.5134329866590429 480.0 41.0 332.0 
182.00042575550555 6194.0 NaN 1539.8877755510998 0.5157131249961209 479.0 41.0 330.0 
184.00042575550555 6158.0 NaN 1539.8423513694063 0.5093646595616663 480.0 40.0 331.0 
186.0001983480089 6153.0 NaN 1539.861055444221 0.5068982128149805 480.0 40.0 331.0 
188.00042575550555 6162.0 NaN 1539.8022712090844 0.506393372231908 481.0 40.0 331.0 
190.00042575550555 6178.0 NaN 1533.7341349365392 0.5108823871366285 479.0 40.0 329.0 
192.00042575550555 6176.0 NaN 1533.6833667334663 0.511107800001176 481.0 39.0 330.0 
194.00042575550555 6163.0 NaN 1536.8737474949912 0.5093253717528582 481.0 39.0 331.0 
196.0001983480089 6138.0 NaN 1536.8550434201725 0.5092705027339598 480.0 40.0 330.0 
198.00042575550555 6141.0 NaN 1533.857047428191 0.5115270669610532 481.0 39.0 330.0 
200.00042575550555 6135.0 NaN 1539.9198396793574 0.5135429119640847 480.0 41.0 330.0 
202.00042575550555 6126.0 NaN 1539.9572478289908 0.5152451494674445 482.0 41.0 330.0 
204.00042575550555 6099.0 NaN 1539.9038076152299 0.5103739324877469 481.0 42.0 329.0 
206.0001983480089 6089.0 NaN 1539.9144956579817 0.5086032063883489 481.0 42.0 329.0 
208.00042575550555 6113.0 NaN 1536.8149632598543 0.5062001085244616 477.0 42.0 328.0 
210.00042575550555 6123.0 NaN 1530.6452905811634 0.5128447565034083 473.0 42.0 326.0 
212.00042575550555 6099.0 NaN 1527.5003340013366 0.511529412654074 473.0 43.0 324.0 
214.00042575550555 6099.0 NaN 1530.6265865063453 0.5089843489718409 474.0 42.0 326.0 
216.0001983480089 6083.0 NaN 1533.694054776218 0.50649944693868 474.0 43.0 326.0 
218.00042575550555 6097.0 NaN 1527.6659986639938 0.512576499140805 474.0 43.0 324.0 
220.00042575550555 6067.0 NaN 1527.5751503006 0.5076775350801703 474.0 41.0 326.0 
222.00042575550555 6061.0 NaN 1524.5718102872424 0.5105615704975502 474.0 41.0 325.0 
224.00042575550555 6034.0 NaN 1521.3627254509004 0.5091769799755093 479.0 36.0 329.0 
226.0001983480089 6031.0 NaN 1524.5424181696726 0.507216577463738 477.0 38.0 328.0 
228.00042575550555 6042.0 NaN 1527.5243820975272 0.5111527354518859 477.0 38.0 329.0 
230.00042575550555 6015.0 NaN 1527.6098864395453 0.5107917234456791 460.0 53.0 314.0 
232.00042575550555 6004.0 NaN 1533.7047428189726 0.5116706451458248 459.0 53.0 316.0 
234.00042575550555 5987.0 NaN 1533.7207748831001 0.5097993864616466 464.0 49.0 320.0 
236.0001983480089 5987.0 NaN 1533.6940547762183 0.5070886208874924 467.0 46.0 323.0 
238.00042575550555 5997.0 NaN 1533.6833667334663 0.5144507724379023 468.0 45.0 324.0 
240.00042575550555 5992.0 NaN 1530.5838343353364 0.5154094002916768 467.0 46.0 322.0 
242.00042575550555 5989.0 NaN 1530.5250501002 0.5090146906820514 470.0 43.0 325.0 
244.00042575550555 5979.0 NaN 1524.3714094856366 0.5089432289655715 470.0 43.0 323.0 
246.0001983480089 5985.0 NaN 1524.416833667336 0.5083777792217734 468.0 45.0 321.0 
248.00042575550555 6000.0 NaN 1527.5190380761544 0.508787111967812 471.0 42.0 325.0 
250.00042575550555 5982.0 NaN 1524.4863059452246 0.5198628640644103 471.0 42.0 324.0 
252.00042575550555 5945.0 NaN 1524.5851703406818 0.5130391829864367 471.0 44.0 322.0 
254.00042575550555 5940.0 NaN 1524.4488977955907 0.513783391239601 469.0 43.0 323.0 
256.0001983480089 5920.0 NaN 1524.5237140948548 0.5067915444854495 471.0 42.0 324.0 
258.00042575550555 5935.0 NaN 1524.4408817635274 0.5088512013799696 473.0 42.0 324.0 
260.00042575550555 5943.0 NaN 1521.3841015364067 0.5090144040435697 473.0 42.0 323.0 
262.00042575550555 5927.0 NaN 1515.2571810287247 0.5129409463477845 472.0 42.0 321.0 
264.00042575550555 5909.0 NaN 1515.2999331997335 0.5113912456310442 470.0 42.0 321.0 
266.0001983480089 5911.0 NaN 1515.2678690714756 0.5077036656422845 470.0 42.0 321.0 
268.00042575550555 5916.0 NaN 1524.5397461589841 0.5152974738036162 471.0 42.0 324.0 
270.00042575550555 5903.0 NaN 1533.7207748831001 0.511795170773056 471.0 42.0 327.0 
272.00042575550555 5886.0 NaN 1530.6666666666663 0.5113644827302881 471.0 42.0 326.0 
274.00042575550555 5875.0 NaN 1533.814295257182 0.5142553437079601 469.0 46.0 323.0 
276.0001983480089 5865.0 NaN 1530.6639946559792 0.5066968747673608 474.0 42.0 326.0 
278.00042575550555 5880.0 NaN 1530.6372745490999 0.5132936966965 466.0 48.0 320.0 
280.00042575550555 5882.0 NaN 1530.7174348697386 0.5076955626655316 472.0 42.0 326.0 
282.00042575550555 5876.0 NaN 1530.6506346025365 0.5147473574479428 476.0 38.0 330.0 
284.00042575550555 5859.0 NaN 1530.5891783567115 0.511188798649571 478.0 38.0 330.0 
286.0001983480089 5844.0 NaN 1530.594522378091 0.507647135786143 477.0 38.0 330.0 
288.00042575550555 5842.0 NaN 1530.5250501002 0.5139834513642921 476.0 38.0 330.0 
290.00042575550555 5835.0 NaN 1533.6913827655294 0.5141890459864465 475.0 38.0 331.0 
292.00042575550555 5815.0 NaN 1533.5978623914477 0.5131828229732484 474.0 38.0 331.0 
294.00042575550555 5811.0 NaN 1533.6860387441545 0.510720414895401 474.0 38.0 331.0 
296.0001983480089 5806.0 NaN 1536.6599866399451 0.5132279219925003 474.0 38.0 332.0 
298.00042575550555 5790.0 NaN 1536.7374749499 0.5075754459125411 473.0 38.0 332.0 
300.00042575550555 5804.0 NaN 1539.7969271877091 0.5106700433228011 473.0 38.0 333.0 
302.00042575550555 5790.0 NaN 1539.8129592518362 0.5187135763995372 473.0 38.0 333.0 
304.00042575550555 5773.0 NaN 1530.6720106880434 0.5168361526875174 473.0 38.0 330.0 
306.0001983480089 5760.0 NaN 1539.8717434869727 0.5051409021154185 472.0 38.0 333.0 
308.00042575550555 5776.0 NaN 1536.7428189712748 0.5114082006113395 470.0 38.0 332.0 
310.00042575550555 5769.0 NaN 1533.7287909151637 0.5140274507826137 470.0 38.0 331.0 
312.00042575550555 5764.0 NaN 1533.6539746159 0.5123904339450707 470.0 38.0 331.0 
314.00042575550555 5763.0 NaN 1533.6138944555794 0.5105817583353099 472.0 38.0 331.0 
316.0001983480089 5764.0 NaN 1530.5223780895114 0.5089528556579835 473.0 38.0 330.0 
318.00042575550555 5782.0 NaN 1536.6973947895794 0.5130408635972611 473.0 38.0 332.0 
320.00042575550555 5769.0 NaN 1530.5784903139615 0.5153425589764702 471.0 38.0 330.0 
322.00042575550555 5756.0 NaN 1530.5651302605208 0.5117616646031993 471.0 38.0 330.0 
324.00042575550555 5759.0 NaN 1536.6920507682023 0.5080496385035792 470.0 38.0 332.0 
326.0001983480089 5763.0 NaN 1536.6038744155 0.5058953382286667 471.0 38.0 332.0 
328.00042575550555 5789.0 NaN 1539.7621910487635 0.5085708788508334 470.0 39.0 332.0 
330.00042575550555 5774.0 NaN 1542.8189712758842 0.5111869357726732 472.0 38.0 334.0 
332.00042575550555 5767.0 NaN 1539.6847027388092 0.5115169415586737 471.0 39.0 332.0 
334.00042575550555 5756.0 NaN 1536.6786907147637 0.5159577725928701 472.0 38.0 332.0 
336.0001983480089 5740.0 NaN 1536.553106212425 0.5075568128189862 473.0 38.0 332.0 
338.00042575550555 5761.0 NaN 1530.5811623246484 0.5114737475878 473.0 38.0 330.0 
340.00042575550555 5775.0 NaN 1530.479625918503 0.5129698214699168 472.0 38.0 330.0 
342.00042575550555 5778.0 NaN 1536.7027388109543 0.5117966057976886 471.0 38.0 332.0 
344.00042575550555 5788.0 NaN 1536.694722778891 0.5199228760595269 475.0 38.0 332.0 
346.0001983480089 5771.0 NaN 1536.6412825651298 0.508961129005546 475.0 38.0 332.0 
348.00042575550555 5777.0 NaN 1533.5577822311275 0.5100343743248393 475.0 38.0 331.0 
350.00042575550555 5777.0 NaN 1530.5010020040093 0.5112795632210119 475.0 38.0 330.0 
352.00042575550555 5788.0 NaN 1530.4956579826332 0.5118884716866122 477.0 38.0 330.0 
354.00042575550555 5790.0 NaN 1527.3854375417511 0.5147405558614765 477.0 38.0 329.0 
356.0001983480089 5767.0 NaN 1527.420173680694 0.5100170165362453 476.0 38.0 329.0 
358.00042575550555 5759.0 NaN 1527.5885103540422 0.5119657123472882 475.0 38.0 329.0 
360.00042575550555 5736.0 NaN 1527.4442217768878 0.5112881457469148 475.0 38.0 329.0 
362.00042575550555 5738.0 NaN 1527.404141616566 0.5118498264529541 476.0 40.0 327.0 
364.00042575550555 5760.0 NaN 1524.3580494321996 0.5150018608341924 473.0 43.0 323.0 
366.0001983480089 5745.0 NaN 1524.384769539079 0.5127525370252752 473.0 43.0 323.0 
368.00042575550555 5757.0 NaN 1505.9665998664 0.5103183564252263 470.0 44.0 316.0 
370.00042575550555 5753.0 NaN 1505.9345357381433 0.5080430022833058 472.0 43.0 317.0 
372.00042575550555 5753.0 NaN 1508.9699398797604 0.5165169480886517 473.0 43.0 318.0 
374.00042575550555 5768.0 NaN 1512.0267201068812 0.5142185241654077 475.0 42.0 320.0 
376.0001983480089 5747.0 NaN 1518.1482965931848 0.51016569094964 476.0 41.0 323.0 
378.00042575550555 5748.0 NaN 1512.1416165664657 0.5069838680232027 476.0 41.0 321.0 
380.00042575550555 5778.0 NaN 1512.0668002672 0.5113878514575148 476.0 41.0 321.0 
382.00042575550555 5778.0 NaN 1509.010020040081 0.5163779327677914 481.0 38.0 323.0 
384.00042575550555 5794.0 NaN 1515.177020708082 0.5150880500619819 476.0 41.0 322.0 
386.0001983480089 5755.0 NaN 1515.1716766867073 0.5157251873211314 479.0 39.0 324.0 
388.00042575550555 5755.0 NaN 1509.1917167668666 0.5054546301789261 473.0 43.0 318.0 
390.00042575550555 5769.0 NaN 1509.1596526386097 0.512322119097045 473.0 44.0 317.0 
392.00042575550555 5769.0 NaN 1509.140948563794 0.507337580840306 475.0 41.0 320.0 
394.00042575550555 5790.0 NaN 1512.1870407481638 0.5100770438134548 477.0 40.0 322.0 
396.0001983480089 5762.0 NaN 1512.2084168336667 0.5094814504702907 474.0 43.0 319.0 
398.00042575550555 5802.0 NaN 1509.1543086172355 0.5073521141118712 479.0 42.0 319.0 
400.00042575550555 5813.0 NaN 1506.0574482297918 0.5098892417485293 482.0 38.0 322.0 
402.00042575550555 5813.0 NaN 1505.9826319305273 0.5071572689229987 480.0 39.0 321.0 
404.00042575550555 5832.0 NaN 1512.125584502337 0.5183646050772754 482.0 38.0 324.0 
406.0001983480089 5784.0 NaN 1515.2438209752825 0.5094991107640633 485.0 38.0 325.0 
408.00042575550555 5800.0 NaN 1521.4829659318634 0.506726704365555 485.0 37.0 328.0 
410.00042575550555 5828.0 NaN 1524.5557782231122 0.5075562497091168 483.0 39.0 327.0 
412.00042575550555 5828.0 NaN 1521.5444221776902 0.5146474612793226 480.0 42.0 323.0 
414.00042575550555 5819.0 NaN 1524.5557782231122 0.5165775756457678 481.0 42.0 324.0 
416.0001983480089 5782.0 NaN 1521.4562458249823 0.5094863752907919 481.0 42.0 323.0 
418.00042575550555 5780.0 NaN 1521.509686038745 0.5222886982209108 482.0 42.0 323.0 
420.00042575550555 5755.0 NaN 1509.1649966599878 0.5201179637185225 487.0 38.0 323.0 
422.00042575550555 5723.0 NaN 1509.0768203072819 0.5138514591812638 486.0 39.0 322.0 
424.00042575550555 5731.0 NaN 1506.0440881763534 0.5133597171167649 482.0 38.0 322.0 
426.0001983480089 5713.0 NaN 1506.0494321977283 0.506239407479199 480.0 38.0 322.0 
428.00042575550555 5739.0 NaN 1505.9639278557104 0.5194903267601507 480.0 38.0 322.0 
430.00042575550555 5755.0 NaN 1499.882431529727 0.5148989373180364 476.0 42.0 316.0 
432.00042575550555 5738.0 NaN 1512.2297929191725 0.5067214753527414 475.0 43.0 319.0 
434.00042575550555 5744.0 NaN 1518.3727454909817 0.5120579835514025 476.0 43.0 321.0 
436.0001983480089 5741.0 NaN 1524.4141616566455 0.5048847374570864 483.0 43.0 323.0 
438.00042575550555 5769.0 NaN 1524.4943219772883 0.5166743007090758 483.0 43.0 323.0 
440.00042575550555 5759.0 NaN 1524.5611222444904 0.5125731125435395 483.0 44.0 322.0 
442.00042575550555 5745.0 NaN 1533.7287909151637 0.5085025452575067 483.0 44.0 325.0 
444.00042575550555 5756.0 NaN 1530.6826987307938 0.514769752291865 485.0 44.0 324.0 
446.0001983480089 5750.0 NaN 1533.6299265197065 0.5123467981375329 485.0 44.0 325.0 
448.00042575550555 5764.0 NaN 1533.7234468937882 0.5128141735057655 483.0 45.0 324.0 
450.00042575550555 5761.0 NaN 1533.6566466265874 0.5119267697725789 484.0 43.0 326.0 
452.00042575550555 5737.0 NaN 1533.7207748831 0.5092108946757116 483.0 43.0 326.0 
454.00042575550555 5759.0 NaN 1542.8189712758845 0.5144718527201333 484.0 42.0 330.0 
456.0001983480089 5743.0 NaN 1545.9639278557092 0.5105120410085687 483.0 44.0 329.0 
458.00042575550555 5755.0 NaN 1542.888443553773 0.5106727595894242 482.0 42.0 330.0 
460.00042575550555 5756.0 NaN 1542.9873079492336 0.5098750132902949 480.0 43.0 329.0 
462.00042575550555 5730.0 NaN 1542.86706746827 0.508143119215868 481.0 44.0 328.0 
464.00042575550555 5741.0 NaN 1536.70808283233 0.5135333107653868 486.0 39.0 331.0 
466.0001983480089 5735.0 NaN 1536.598530394121 0.5100327751573867 482.0 44.0 326.0 
468.00042575550555 5751.0 NaN 1536.6626586506359 0.510763842117143 480.0 44.0 326.0 
470.00042575550555 5743.0 NaN 1530.4742818971272 0.5081931581786939 481.0 42.0 326.0 
472.00042575550555 5733.0 NaN 1527.4762859051452 0.5067070214736861 478.0 42.0 325.0 
474.00042575550555 5741.0 NaN 1524.2885771543092 0.5163705408155649 477.0 42.0 324.0 
476.0001983480089 5714.0 NaN 1521.2611890447552 0.5073310836872551 477.0 41.0 324.0 
478.00042575550555 5738.0 NaN 1524.4008016032067 0.5165088274449785 476.0 42.0 324.0 
480.00042575550555 5734.0 NaN 1527.4736138944545 0.512523835834982 475.0 42.0 325.0 
482.00042575550555 5721.0 NaN 1518.2792251168999 0.5102622981154449 475.0 41.0 323.0 
484.00042575550555 5726.0 NaN 1518.263193052773 0.5172722209507621 476.0 41.0 323.0 
486.0001983480089 5697.0 NaN 1515.1983967935878 0.5099378985612238 473.0 40.0 323.0 
488.00042575550555 5700.0 NaN 1506.0387441549758 0.5120121367816547 470.0 41.0 319.0 
490.00042575550555 5704.0 NaN 1506.0040080160334 0.5076951499699639 470.0 41.0 319.0 
492.00042575550555 5702.0 NaN 1499.9038076152297 0.5132635685368312 468.0 41.0 317.0 
494.00042575550555 5705.0 NaN 1490.7014028056103 0.5135315827367428 470.0 41.0 314.0 
496.0001983480089 5686.0 NaN 1490.6666666666658 0.5073125732419598 467.0 41.0 314.0 
498.00042575550555 5695.0 NaN 1493.7100868403465 0.5114623366265117 466.0 40.0 316.0 
500.00042575550555 5697.0 NaN 1493.7394789579164 0.51822464942988 470.0 40.0 316.0 
502.00042575550555 5695.0 NaN 1493.7822311289253 0.5036264631638491 471.0 40.0 316.0 
504.00042575550555 5708.0 NaN 1490.6907147628576 0.5111065627340635 473.0 40.0 315.0 
506.0001983480089 5692.0 NaN 1493.6887107548437 0.5063826654639533 472.0 40.0 316.0 
508.00058376331435 5709.0 NaN 1490.6960587842357 0.5112305279618392 477.0 40.0 315.0 
510.00058376331435 5701.0 NaN 1493.6753507014018 0.5063434190604595 476.0 40.0 316.0 
512.0005837633144 5700.0 NaN 1487.6098864395453 0.5113584830248415 475.0 40.0 314.0 
514.0005837633144 5712.0 NaN 1487.5751503006002 0.5141773479304919 474.0 40.0 314.0 
516.0001983480089 5698.0 NaN 1484.5878423513705 0.5067064921295311 472.0 40.0 313.0 
518.0005837633144 5706.0 NaN 1478.4261857047434 0.5092558316437167 468.0 42.0 309.0 
520.0005837633144 5702.0 NaN 1478.4101536406154 0.5110838497798412 468.0 40.0 311.0 
522.0005837633144 5689.0 NaN 1475.2705410821638 0.510021176260419 465.0 41.0 309.0 
524.0005837633144 5697.0 NaN 1459.938543754174 0.5099420465348232 466.0 40.0 305.0 
526.0001983480089 5702.0 NaN 1462.9498997995997 0.5111952052149009 462.0 41.0 305.0 
528.0005837633144 5713.0 NaN 1463.019372077489 0.5100447954654315 462.0 39.0 307.0 
530.0005837633144 5721.0 NaN 1462.9632598530388 0.5139040004497707 460.0 39.0 307.0 
532.0005837633144 5702.0 NaN 1463.0781563126259 0.508791778466993 463.0 41.0 305.0 
534.0005837633144 5695.0 NaN 1466.030728122912 0.5074097813041551 467.0 38.0 309.0 
536.0001983480089 5694.0 NaN 1466.041416165664 0.5073670398802383 467.0 39.0 308.0 
538.0005837633144 5703.0 NaN 1466.0494321977283 0.5062871223601298 469.0 39.0 308.0 
540.0005837633144 5716.0 NaN 1466.0708082832339 0.5121454383249466 471.0 38.0 309.0 
542.0005837633144 5712.0 NaN 1469.0875083500339 0.5074340465698005 474.0 38.0 310.0 
544.0005837633144 5716.0 NaN 1462.963259853039 0.5170100965443251 475.0 38.0 308.0 
546.0001983480089 5705.0 NaN 1462.973947895791 0.5067169784896437 475.0 38.0 308.0 
548.0005837633144 5717.0 NaN 1466.0975283901125 0.5061305631372366 475.0 38.0 309.0 
550.0005837633144 5716.0 NaN 1463.0113560454247 0.5199390698894716 473.0 39.0 307.0 
552.0005837633144 5692.0 NaN 1472.2378089512363 0.5121947890189744 473.0 39.0 310.0 
554.0005837633144 5727.0 NaN 1472.2484969939883 0.5109900674882218 475.0 38.0 311.0 
556.0001983480089 5708.0 NaN 1469.15965263861 0.5101314306931475 475.0 38.0 310.0 
558.0005837633144 5708.0 NaN 1466.108216432865 0.5043932258449197 475.0 38.0 309.0 
560.0005837633144 5723.0 NaN 1462.9579158316637 0.5111905858893193 473.0 38.0 308.0 
562.0005837633144 5720.0 NaN 1462.9712758851035 0.5131417713975265 466.0 43.0 303.0 
564.0005837633144 5772.0 NaN 1466.0681362725454 0.5123067120115856 467.0 44.0 303.0 
566.0001983480089 5740.0 NaN 1466.0547762191054 0.513929857964062 469.0 43.0 304.0 
568.0005837633144 5729.0 NaN 1466.0681362725454 0.5052204998120013 467.0 44.0 303.0 
570.0005837633144 5736.0 NaN 1469.1943887775547 0.5116942363570521 471.0 43.0 305.0 
572.0005837633144 5737.0 NaN 1478.4475617902467 0.5138430805401403 469.0 44.0 307.0 
574.0005837633144 5744.0 NaN 1481.600534402137 0.5364747132664883 466.0 45.0 307.0 
576.0001983480089 5677.0 NaN 1481.5551102204397 0.5077463809890145 475.0 38.0 314.0 
578.0005837633144 5688.0 NaN 1481.5257181028728 0.5093941205587446 475.0 38.0 314.0 
580.0005837633144 5699.0 NaN 1484.643954575818 0.512939033995237 477.0 39.0 314.0 
582.0005837633144 5699.0 NaN 1481.5845023380089 0.5141135911210385 475.0 37.0 315.0 
584.0005837633144 5726.0 NaN 1493.8677354709425 0.5310055687509108 479.0 37.0 319.0 
586.0001983480089 5682.0 NaN 1490.7575150300609 0.5156971909611531 482.0 37.0 318.0 
588.0005837633144 5658.0 NaN 1487.6499665998656 0.5039861709697537 481.0 38.0 316.0 
590.0005837633144 5686.0 NaN 1490.6346025384094 0.5112938513236406 484.0 37.0 318.0 
592.0005837633144 5688.0 NaN 1493.832999331998 0.5118746915538103 484.0 37.0 319.0 
594.0005837633144 5715.0 NaN 1490.685370741482 0.5177757557238087 485.0 37.0 318.0 
596.0001983480089 5682.0 NaN 1490.7201068804272 0.518154294234329 483.0 37.0 318.0 
598.0005837633144 5650.0 NaN 1493.7341349365395 0.5101431796209666 484.0 37.0 319.0 
600.0005837633144 5665.0 NaN 1499.919839679358 0.5097005994799425 491.0 37.0 321.0 
602.0005837633144 5673.0 NaN 1499.8610554442214 0.5175452679963113 490.0 37.0 321.0 
604.0005837633144 5734.0 NaN 1499.9492317969275 0.5224928717195394 493.0 37.0 321.0 
606.0001983480089 5710.0 NaN 1499.8129592518364 0.5071294603648747 492.0 38.0 320.0 
608.0005837633144 5703.0 NaN 1496.8069472277896 0.5028365574112774 489.0 41.0 316.0 
610.0005837633144 5722.0 NaN 1502.9686038744164 0.5111357649336585 491.0 39.0 320.0 
612.0005837633144 5715.0 NaN 1506.0387441549758 0.5149090374252883 480.0 47.0 313.0 
614.0005837633144 5763.0 NaN 1518.362057448229 0.5205460195433085 483.0 43.0 321.0 
616.0001983480089 5742.0 NaN 1518.4555778223103 0.5095801386452243 480.0 49.0 315.0 
618.0005837633144 5759.0 NaN 1515.2277889111544 0.5042324547823878 477.0 49.0 314.0 
620.0005837633144 5813.0 NaN 1512.3019372077486 0.5115549948214261 476.0 49.0 313.0 
622.0005837633144 5825.0 NaN 1515.241148964597 0.507656761628645 480.0 45.0 318.0 
624.0005837633144 5883.0 NaN 1518.4288577154302 0.518703339361312 481.0 43.0 321.0 
626.0001983480089 5848.0 NaN 1518.2792251169 0.5081098476469402 483.0 40.0 324.0 
628.0005837633144 5861.0 NaN 1524.5771543086182 0.5123082697405265 486.0 40.0 326.0 
630.0005837633144 5857.0 NaN 1527.6098864395453 0.5189104239355834 487.0 43.0 324.0 
632.0005837633144 5812.0 NaN 1521.4856379425528 0.51082329815164 489.0 41.0 324.0 
634.0005837633144 5855.0 NaN 1524.488977955911 0.521174874462109 492.0 41.0 325.0 
636.0001983480089 5799.0 NaN 1524.5824983299926 0.5087862935270352 496.0 41.0 325.0 
638.0005837633144 5812.0 NaN 1524.5290581162333 0.5067551536500287 490.0 45.0 321.0 
640.0005837633144 5845.0 NaN 1518.4395457581825 0.509512324569237 491.0 41.0 323.0 
642.0005837633144 5840.0 NaN 1518.3380093520366 0.512241869172732 485.0 43.0 321.0 
644.0005837633144 5860.0 NaN 1518.34869739479 0.5226409428767808 482.0 45.0 319.0 
646.0001983480089 5824.0 NaN 1521.3493653974624 0.5078205007694638 484.0 43.0 322.0 
648.0005837633144 5831.0 NaN 1521.4562458249827 0.5105949321040831 485.0 42.0 323.0 
650.0004257555055 5854.0 NaN 1518.300601202406 0.5143369178344677 484.0 43.0 321.0 
652.0004257555055 5824.0 NaN 1524.4569138276547 0.5077477310863137 482.0 42.0 324.0 
654.0004257555055 5846.0 NaN 1524.5477621910477 0.5158167187505365 482.0 41.0 325.0 
656.0001983480089 5811.0 NaN 1527.553774215098 0.5078557140600993 485.0 37.0 330.0 
658.0004257555055 5819.0 NaN 1527.4976619906472 0.5104577096813037 484.0 37.0 330.0 
660.0004257555055 5847.0 NaN 1530.5704742818982 0.5204729748462018 484.0 37.0 331.0 
662.0004257555055 5791.0 NaN 1530.6105544422182 0.5113581558440714 484.0 37.0 331.0 
664.0004257555055 5831.0 NaN 1542.9659318637273 0.5191065174131824 481.0 37.0 335.0 
#####################################################################
# End of data export. Simulation finished at: 2019-03-16T04:56+0000 #
#####################################################################
